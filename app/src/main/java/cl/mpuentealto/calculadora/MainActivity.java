package cl.mpuentealto.calculadora;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.mariuszgromada.math.mxparser.*;

public class MainActivity extends AppCompatActivity {

    private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btnMas, btnMenos, btnPorcentaje, btnMultiplicar, btnDividir, btnIgual, btnBorrar;
    private EditText editText;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Toast toast = Toast.makeText(this, "Iniciando...", Toast.LENGTH_SHORT);
        toast.show();



        btnIgual = ((Button) findViewById(R.id.btnIgual));
        btn1 = ((Button) findViewById(R.id.btn1));
        btn2 = ((Button) findViewById(R.id.btn2));
        btn3 = ((Button) findViewById(R.id.btn3));
        btn4 = ((Button) findViewById(R.id.btn4));
        btn5 = ((Button) findViewById(R.id.btn5));
        btn6 = ((Button) findViewById(R.id.btn6));
        btn7 = ((Button) findViewById(R.id.btn7));
        btn8 = ((Button) findViewById(R.id.btn8));
        btn9 = ((Button) findViewById(R.id.btn9));
        btn0 = ((Button) findViewById(R.id.btn0));
        btnMas = ((Button) findViewById(R.id.btnMas));
        btnMenos = ((Button) findViewById(R.id.btnMenos));
        btnDividir = ((Button) findViewById(R.id.btnDividir));
        btnMultiplicar = ((Button) findViewById(R.id.btnMultiplicar));
        btnPorcentaje = ((Button) findViewById(R.id.btnPorcentaje));
        btnBorrar = ((Button) findViewById(R.id.btnBorrar));
        editText = (EditText) findViewById((R.id.editText));

        editText.setShowSoftInputOnFocus(false);

        btnIgual.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                calcular();
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                borrar();
            }
        });
    }

    public void calcular(){
        Expression e = new Expression(editText.getText().toString());
        mXparser.consolePrintln("Res: " + e.getExpressionString() + " = " + e.calculate());
        if (e.calculate() % 1 == 0){
            String res = String.valueOf(((int) e.calculate()));
            editText.setText(res);
        }else{
            String res = String.valueOf(e.calculate());
            editText.setText(res);
        }
        editText.setSelection(editText.length());
    }

    public void borrar(){
        int pos = editText.getSelectionStart();
        if (pos > 0) {
            String text = editText.getText().delete(pos - 1, pos).toString();
            editText.setText(text);
            editText.setSelection(pos -1);
        }
    }

    public void addText(View v){
        Button b = (Button) v;
        int pos = editText.getSelectionStart();
        editText.setText(editText.getText().toString() + b.getText().toString());
        editText.setSelection(pos + 1);
    }
}
